{
"default_popups_fouet" : "Character graphics|Whip",
"default_popups_fouet-inversee" : "Character graphics|Whip (Reversed)",

"default_popups_guilty" : "Events|Guilty",
"default_popups_cross-examination" : "Events|Cross-examination",
"default_popups_not-guilty" : "Events|Not Guilty",
"default_popups_witness-testimony" : "Events|Witness testimony",
"default_popups_testimony-statement" : "Events|Testimony statement",
"default_popups_game-over-doors" : "Events|Game Over doors",
"default_popups_unlock-successful" : "Events|Unlock successful",

"default_popups_eureka" : "Bubbles|Eureka!",
"default_popups_gotcha" : "Bubbles|Gotcha!",
"default_popups_hold-it" : "Bubbles|Hold it!",
"default_popups_not-so-fast" : "Bubbles|Not so fast!",
"default_popups_objection" : "Bubbles|Objection!",
"default_popups_take-that" : "Bubbles|Take that!",

"default_popups_contre-interrogatoire" : "French Graphics|Cross-examination",
"default_popups_j-te-tiens" : "French Graphics|Gotcha!",
"default_popups_coupable" : "French Graphics|Guilty",
"default_popups_un-instant" : "French Graphics|Hold it!",
"default_popups_non-coupable" : "French Graphics|Not Guilty",
"default_popups_prends-ca" : "French Graphics|Take that!",
"default_popups_deposition-du-temoin" : "French Graphics|Witness testimony",

"default_popups_aussage" : "German Graphics|Testimony statement",
"default_popups_einspruch" : "German Graphics|Objection!",
"default_popups_ertappt" : "German Graphics|Gotcha!",
"default_popups_kreuzverhor" : "German Graphics|Cross-examination",
"default_popups_moment-mal" : "German Graphics|Hold it!",
"default_popups_nimm-das" : "German Graphics|Take that!",
"default_popups_schuldig" : "German Graphics|Guilty",
"default_popups_zeugenaussage" : "German Graphics|Witness testimony",

"default_popups_te-tengo" : "Spanish Graphics|Gotcha!",
"default_popups_un-momento" : "Spanish Graphics|Hold it!",
"default_popups_protesto" : "Spanish Graphics|Objection!",
"default_popups_toma-ya" : "Spanish Graphics|Take that!"
}
