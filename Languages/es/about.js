{
	"about_aao" : "Sobre Ace Attorney Online",
	
	"aao_core_team" : "Equipo central",
	
	"aao_role_unas" : "Fundador, Desarrollador, Administrador",
	"aao_role_meph" : "Gestor de la comunidad Inglesa, Diseñador",
	"aao_role_kroki" : "Gestor de la comunidad Francesa",
	"aao_role_daniel" : "Gestor de la comunidad Española",
	"aao_role_thepasch" : "Gestor de la comunidad Alemana",
	
	"aao_supporting_teams" : "Equipos de apoyo",
	
	"aao_mods" : "Moderadores",
	"aao_qa_en" : "Evaluadores Ingleses",
	"aao_qa_fr" : "Evaluadores Franceses",
	"aao_qa_es" : "Evaluadores Españoles",
	"aao_qa_de" : "Evaluadores Alemanes",
	
	"aao_special_thanks" : "Agradecimientos",
	
	"aao_role_spparrow" : "Fundador, Concepto Original",
	"aao_role_devsupport" : "Apoyo en el desarrollo",
	"aao_role_translation" : "Apoyo en la traducción",
	"aao_role_music" : "Compositores Originales",
	"aao_baotl_winner" : "Ganador de la competencia \"La Era Dorada de la Ley\""
}
