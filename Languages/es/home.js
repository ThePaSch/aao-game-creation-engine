{
	"homepage" : "Página de inicio",
	
	"welcome" : "Bienvenido a Ace Attorney Online, el creador de casos online!",
	"site_presentation" : "AAO es el primer sitio para jugar, crear y compartir aventuras, usando elementos de la saga Ace Attorney de Capcom.",
	"no_download" : "Sin descargas, sin molestias - regístrate y empieza a escribir tu propio juego desde tu navegador favorito!",
	
	"news" : "Últimas noticias y actualizaciones",
	"posted_on" : "Escrito el <date> a la hora <time>",
	"view_comments" : "Ver comentarios",
	
	"random_featured" : "Juego Favorito",
	
	"affiliates" : "Afiliados",
	"contact" : "Contáctanos"
}
